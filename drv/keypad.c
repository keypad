/*******************************************************************************
 *    File Name : keypad.c
 * 
 *       Author : Henry He
 * Created Time : Fri 20 Nov 2009 01:17:12 PM CST
 *  Description : 4*4键盘扫描模块，带键盘去抖
 ******************************************************************************/


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include <linux/module.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <asm/hardware.h>
#include <asm/delay.h>
#include <asm/uaccess.h>
#include <asm/io.h>
#include <linux/wait.h>
#include <linux/poll.h>
#include <linux/cdev.h>
#include <asm-arm/irq.h>
#include <linux/interrupt.h>
#include <asm-arm/arch-s3c2410/irqs.h>
#include <asm-arm/arch-s3c2410/regs-gpio.h>


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/


//===========================  Pin  ============================================

#define KEYSCAN0       S3C_GPL0
#define KEYSCAN0_OUTP  S3C_GPL0_OUTP
#define KEYSCAN1       S3C_GPL1
#define KEYSCAN1_OUTP  S3C_GPL1_OUTP
#define KEYSCAN2       S3C_GPL2
#define KEYSCAN2_OUTP  S3C_GPL2_OUTP
#define KEYSCAN3       S3C_GPL3
#define KEYSCAN3_OUTP  S3C_GPL3_OUTP

#define KEYROW0        S3C_GPK8
#define KEYROW0_INP    S3C_GPK8_INP
#define KEYROW1        S3C_GPK9
#define KEYROW1_INP    S3C_GPK9_INP
#define KEYROW2        S3C_GPK10
#define KEYROW2_INP    S3C_GPK10_INP
#define KEYROW3        S3C_GPK11
#define KEYROW3_INP    S3C_GPK11_INP


//===========================  Status  =========================================

#define STATUS_NOKEY   0                          //无按键状态，按键抬起
#define STATUS_DOWNX   1                          //有按键状态，但不确定
#define STATUS_DOWN    2                          //等释放状态，确定按下



//===========================  Buffer  =========================================

#define BUF_HEAD       (s_zkdKeyDev.m_aucBuf[s_zkdKeyDev.m_unBufHead])
#define BUF_TAIL       (s_zkdKeyDev.m_aucBuf[s_zkdKeyDev.m_unBufTail])
#define INCBUF(x,mod)  ((++(x)) & ((mod)-1))
#define MAX_BUTTON_BUF 16                         //按键缓冲区大小



//===========================  Keys  ===========================================

// Invalidate key value
#define INVALID_KEY              ( (char)(-1) )

// Count of key in downx status before enter down status
#define KEY_DOWNX_CNT            (3)

// Count of key in down status before enter no key status (approcimately 2s)
#define KEY_DOWN_CNT             (50*2)

// Time before pin signal stable in us
#define GET_KEY_DELAY_US         (2)

// Interval for get key timer
#define NEXT_GET_KEY_JIFFIES     (jiffies + msecs_to_jiffies(20))

//===========================  Device  =========================================

#define DEVICE_NAME    "keypad"                 // 设备名称



//===========================  Debug  ==========================================
#define KEYPAD_DEBUGF(arg...) printk (arg)


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/
typedef struct semaphore Semaphore;
typedef struct cdev      CharDev;

typedef struct __tagKeyDev {
  Semaphore         m_smMutex;                    // Mutex for this driver
  CharDev           m_cdDev;                      // cdev结构体
  bool              m_bOpened;
  char              m_cLastKey;                   // Last key scaned
  unsigned int      m_unKeyStatus;                // Key status
  unsigned int      m_unKeyCnt;                   // Count of last key
  unsigned char     m_aucBuf [MAX_BUTTON_BUF];    // Key buffer
  unsigned int      m_unBufHead;                  // Buffer header
  unsigned int      m_unBufTail;                  // Buffer tail
  wait_queue_head_t m_wqRead;                     // 等待队列
  struct timer_list m_tlKeyTimer;                 // 按键去抖定时器
  Semaphore         m_smReadMutex;                // Mutex for read
} KeyDev;


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : File Variables
 ******************************************************************************/

// 主设备号
static int     s_nKeyMajor = 200;
static KeyDev  s_zkdKeyDev;

// Keypad Map
static char s_acKeyTable [][3] = {
  {'1', '2', '3',},
  {'4', '5', '6',},
  {'7', '8', '9',},
  {'*', '0', '#',},
  {INVALID_KEY, INVALID_KEY, INVALID_KEY},
};



/******************************************************************************
 * Desc : 
 ******************************************************************************/
static void InitGPIO(void)
{
  s3c_gpio_cfgpin (KEYSCAN0, KEYSCAN0_OUTP);
  s3c_gpio_cfgpin (KEYSCAN1, KEYSCAN0_OUTP);
  s3c_gpio_cfgpin (KEYSCAN2, KEYSCAN0_OUTP);
  s3c_gpio_cfgpin (KEYSCAN3, KEYSCAN0_OUTP);
  s3c_gpio_setpin (KEYSCAN0, 1);
  s3c_gpio_setpin (KEYSCAN1, 0);
  s3c_gpio_setpin (KEYSCAN2, 1);
  s3c_gpio_setpin (KEYSCAN3, 1);

  s3c_gpio_cfgpin (KEYROW0, KEYROW0_INP);
  s3c_gpio_cfgpin (KEYROW1, KEYROW0_INP);
  s3c_gpio_cfgpin (KEYROW2, KEYROW0_INP);
  s3c_gpio_cfgpin (KEYROW3, KEYROW0_INP);
}


// 扫描时GPIO配置函数
static void InitGPIOScan(void)
{
  // Configure scan port as output
  s3c_gpio_cfgpin(KEYSCAN0, KEYSCAN0_OUTP);
  s3c_gpio_cfgpin(KEYSCAN1, KEYSCAN0_OUTP);
  s3c_gpio_cfgpin(KEYSCAN2, KEYSCAN0_OUTP);
  s3c_gpio_cfgpin(KEYSCAN3, KEYSCAN0_OUTP);

  // Configure row port as input
  s3c_gpio_cfgpin(KEYROW0, KEYROW0_INP);
  s3c_gpio_cfgpin(KEYROW1, KEYROW0_INP);
  s3c_gpio_cfgpin(KEYROW2, KEYROW0_INP);
  s3c_gpio_cfgpin(KEYROW3, KEYROW0_INP);
}



static char GetKey (void)
{
  int i, j=0;

  for (i=0; i<4; i++) {
    if ( i == 0 ) s3c_gpio_setpin (KEYSCAN0, 0); else s3c_gpio_setpin (KEYSCAN0, 1);
    if ( i == 1 ) s3c_gpio_setpin (KEYSCAN1, 0); else s3c_gpio_setpin (KEYSCAN1, 1);
    if ( i == 2 ) s3c_gpio_setpin (KEYSCAN2, 0); else s3c_gpio_setpin (KEYSCAN2, 1);
    if ( i == 3 ) s3c_gpio_setpin (KEYSCAN3, 0); else s3c_gpio_setpin (KEYSCAN3, 1);

    udelay (GET_KEY_DELAY_US);

    if      ( ! s3c_gpio_getpin (KEYROW0) ) { j = 0; break; }
    else if ( ! s3c_gpio_getpin (KEYROW1) ) { j = 1; break; }
    else if ( ! s3c_gpio_getpin (KEYROW2) ) { j = 2; break; }
    //else if ( ! s3c_gpio_getpin (KEYROW3) ) { j = 3; break; }
  }

  return s_acKeyTable [i][j];
}


static void PutKeyEvent (char key)
{
  KEYPAD_DEBUGF ("PutKeyEvent: %c\n", key);

  BUF_HEAD = key;
  s_zkdKeyDev.m_unBufHead = INCBUF (s_zkdKeyDev.m_unBufHead, MAX_BUTTON_BUF);
  wake_up_interruptible (&s_zkdKeyDev.m_wqRead);
}


static char ReadKeyEvent (void)
{
  char cKey = INVALID_KEY;

  if (s_zkdKeyDev.m_unBufHead != s_zkdKeyDev.m_unBufTail) {
    cKey = BUF_TAIL;
    s_zkdKeyDev.m_unBufTail = INCBUF(s_zkdKeyDev.m_unBufTail, MAX_BUTTON_BUF);
  }

  return cKey;
}

static bool IsReadReady (void)
{
  return (s_zkdKeyDev.m_unBufHead != s_zkdKeyDev.m_unBufTail);
}



static void KeyPad_TimerHandler (unsigned long unArg)
{
  char cKey;

  // Scan for a key
  InitGPIOScan ();
  cKey = GetKey ();

  switch (s_zkdKeyDev.m_unKeyStatus) {

    case STATUS_NOKEY:
      if (cKey != INVALID_KEY) {
        // Enter STATUS_DOWNX status
        s_zkdKeyDev.m_cLastKey = cKey;
        s_zkdKeyDev.m_unKeyCnt = KEY_DOWNX_CNT;
        s_zkdKeyDev.m_unKeyStatus = STATUS_DOWNX;
        KEYPAD_DEBUGF ("KeyPad_TimerHandler: scan key %c (not stable)\n", cKey);
      }
      break;

    case STATUS_DOWNX:
      // Check if key in stable status
      if (cKey != s_zkdKeyDev.m_cLastKey) {
        // Key not in stable status
        s_zkdKeyDev.m_unKeyStatus = STATUS_NOKEY;
        KEYPAD_DEBUGF ("KeyPad_TimerHandler: scan key %c unstable (count = %u)\n", cKey, KEY_DOWNX_CNT-s_zkdKeyDev.m_unKeyCnt);
      } else {
        // Key keep stable
        if (--s_zkdKeyDev.m_unKeyCnt == 0) {
          // Key stable, accept it as down
          s_zkdKeyDev.m_unKeyCnt = KEY_DOWN_CNT;
          s_zkdKeyDev.m_unKeyStatus = STATUS_DOWN;

          // 记录键值，唤醒等待队列
          KEYPAD_DEBUGF ("KeyPad_TimerHandler: scan key %c stable\n", cKey);
          PutKeyEvent (cKey);
        }
      }
      break;

    case STATUS_DOWN:
      // Check if key keep press
      if (cKey != s_zkdKeyDev.m_cLastKey) {
        // Key release
        s_zkdKeyDev.m_unKeyStatus = STATUS_NOKEY;
      } else {
        // Key keep press
        if (--s_zkdKeyDev.m_unKeyCnt == 0) {
          // Keep keep press for a long time, treated as another key press
          KEYPAD_DEBUGF ("KeyPad_TimerHandler: re-put key %c\n", cKey);
          PutKeyEvent (cKey);
          s_zkdKeyDev.m_unKeyCnt = KEY_DOWN_CNT;
        }
      }
      break;

  }

  // Modify timer for scanning keys, schedule every 20+ms
  mod_timer ( &s_zkdKeyDev.m_tlKeyTimer, NEXT_GET_KEY_JIFFIES );

}


static ssize_t KeyPad_Read (struct file *filp, char *buffer, size_t count, loff_t *ppos)
{
  int  nRetVal = 0;
  char cKey;

  // Lock this driver
  if ( down_interruptible ( &s_zkdKeyDev.m_smReadMutex ) )
    return -ERESTARTSYS;


  // 当前循环队列中有数据
  cKey = ReadKeyEvent ();

copy_key:

  if ( cKey != INVALID_KEY ) {

    nRetVal = copy_to_user (buffer, &cKey, sizeof(cKey));
    if (nRetVal < 0) {
      nRetVal = -EINVAL;
      goto finished;
    }

    nRetVal = sizeof(cKey);
    goto finished;

  } else {

    // 若用户采用非阻塞方式读取
    if (filp->f_flags & O_NONBLOCK) {
      nRetVal = -EAGAIN;
      goto finished;
    }

    // 采用阻塞方式读取
    nRetVal = wait_event_interruptible (  s_zkdKeyDev.m_wqRead,
        ( cKey = ReadKeyEvent () ) != INVALID_KEY  );

    if (nRetVal != -ERESTARTSYS) {
      goto copy_key;
    }

  }

finished:
  // Unlock this driver
  up ( &s_zkdKeyDev.m_smReadMutex );


  return nRetVal;
}



static unsigned int KeyPad_Poll (struct file *filp, poll_table *wait)

{
  unsigned int unMask = 0;


  if ( down_interruptible ( &s_zkdKeyDev.m_smMutex ) )
    return -ERESTARTSYS;

  if ( IsReadReady () ) {
    // Readable
    unMask |= POLLIN | POLLRDNORM;
  } else {
    poll_wait (filp, &s_zkdKeyDev.m_wqRead,  wait);
  }

  up ( &s_zkdKeyDev.m_smMutex );

  return unMask;
}


static int KeyPad_Open (struct inode *inode,struct file *filp)
{
  // Lock this driver
  if ( down_interruptible ( &s_zkdKeyDev.m_smMutex ) )
    return -ERESTARTSYS;

  if (s_zkdKeyDev.m_bOpened) {
    // Unlock this driver
    up ( &s_zkdKeyDev.m_smMutex );
    return -EBUSY;
  }

  //清空按键动作缓冲区
  s_zkdKeyDev.m_unBufHead = s_zkdKeyDev.m_unBufTail = 0;

  // Add timer for scanning keys, schedule every 20+ms
  s_zkdKeyDev.m_tlKeyTimer.expires = NEXT_GET_KEY_JIFFIES;
  add_timer ( &s_zkdKeyDev.m_tlKeyTimer );

  // Notify that this driver opened
  s_zkdKeyDev.m_bOpened = true;

  // Unlock this driver
  up ( &s_zkdKeyDev.m_smMutex );

  return 0;
}


static int KeyPad_Release (struct inode *inode,struct file *filp)
{
  // Lock this driver
  if ( down_interruptible ( &s_zkdKeyDev.m_smMutex ) )
    return -ERESTARTSYS;

  // Delete timer for scanning keys
  del_timer ( &s_zkdKeyDev.m_tlKeyTimer );

  // Notify that this driver opened
  s_zkdKeyDev.m_bOpened = false;

  // Unlock this driver
  up ( &s_zkdKeyDev.m_smMutex );
  return 0;
}



static struct file_operations foKeyFileOps = {
  .owner   = THIS_MODULE,
  .open    = KeyPad_Open,                       // 启动设备
  .read    = KeyPad_Read,                       // 关闭设备
  .poll    = KeyPad_Poll,                       // Poll for event
  .release = KeyPad_Release,                    // 读取按键值
};


static void SetupCharDev(KeyDev *dev, int minor,
    struct file_operations *fops)
{
  int err;
  int devno;

  devno = MKDEV (s_nKeyMajor, minor);
  cdev_init (&dev->m_cdDev, fops);

  dev->m_cdDev.owner = THIS_MODULE;
  dev->m_cdDev.ops = fops;
  err = cdev_add (&dev->m_cdDev, devno, 1);
  if (err)
    printk ("Error %d adding key%d", err, minor);
}


static int __init KeyPad_Init (void)
{
  int   result;
  dev_t dev;

  KEYPAD_DEBUGF ("KeyPad_Init\n");

  // Initialize and lock this driver
  init_MUTEX_LOCKED (&s_zkdKeyDev.m_smMutex);

  // Initialize read mutex
  init_MUTEX ( &s_zkdKeyDev.m_smReadMutex);

  // Initialize driver open state
  s_zkdKeyDev.m_bOpened = false;


  // 申请设备号
  dev = MKDEV (s_nKeyMajor, 0);

  if (s_nKeyMajor) {
    result = register_chrdev_region (dev, 1, DEVICE_NAME);
  } else {
    result = alloc_chrdev_region (&dev, 0, 1, DEVICE_NAME);
    s_nKeyMajor = MAJOR (dev);
  }

  if (result < 0) {
    printk("key_device: unable to get major %d\n", s_nKeyMajor);
    up ( &s_zkdKeyDev.m_smMutex );
    return result;
  }

  if (s_nKeyMajor == 0)
    s_nKeyMajor = result;

  // 添加cdev
  SetupCharDev (&s_zkdKeyDev, 0, &foKeyFileOps);

  //初始化GPIO设置
  InitGPIO ();

  //初始化按键缓冲区
  s_zkdKeyDev.m_unBufHead = s_zkdKeyDev.m_unBufTail = 0;

  //初始化按键状态
  s_zkdKeyDev.m_unKeyStatus = STATUS_NOKEY;

  //初始化等待队列
  init_waitqueue_head (&s_zkdKeyDev.m_wqRead);

  //初始化定时器，实现软件去抖
  init_timer (&s_zkdKeyDev.m_tlKeyTimer);
  s_zkdKeyDev.m_tlKeyTimer.function = KeyPad_TimerHandler;

  // Unlock this driver
  up ( &s_zkdKeyDev.m_smMutex );

  return 0;
}


static void __exit KeyPad_Exit(void)
{
  KEYPAD_DEBUGF ("KeyPad_Exit\n");
  // Lock this driver
  down_interruptible ( &s_zkdKeyDev.m_smMutex );

  // 删除cdev
  cdev_del (&s_zkdKeyDev.m_cdDev);

  // 释放设备号
  unregister_chrdev_region ( MKDEV(s_nKeyMajor, 0), 1 );

  // Unlock this driver
  up ( &s_zkdKeyDev.m_smMutex );
}


/******************************************************************************
 * Desc : Module Info
 ******************************************************************************/
MODULE_LICENSE ("GPL");
MODULE_AUTHOR  ("Henry He");
MODULE_LICENSE ("GPL");
MODULE_DESCRIPTION ("4x4 KeyPad device driver");


module_init (KeyPad_Init);
module_exit (KeyPad_Exit);


