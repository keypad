/*******************************************************************************
 *    File Name : keypad_test.c
 * 
 *       Author : Henry He
 * Created Time : Fri 27 Nov 2009 09:55:20 AM CST
 *  Description : 
 ******************************************************************************/


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <signal.h>


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : File Variables
 ******************************************************************************/
static int  fdKeyPad = -1;

/******************************************************************************
 * Func : 
 * Desc : 
 * Args : 
 * Outs : 
 ******************************************************************************/
void SignalHandler (int nSignal)
{
  (void)nSignal;
  close (fdKeyPad);
  printf ("SignalHandler: exit\n");
  exit (1);
}

/******************************************************************************
 * Func : 
 * Desc : 
 * Args : 
 * Outs : 
 ******************************************************************************/
int main ( int argc, char *argv[] )
{
  int  nRetVal;
  char cKey;

  (void)argc;
  (void)argv;

  signal (SIGKILL, SignalHandler);
  signal (SIGTERM, SignalHandler);
  signal (SIGHUP, SignalHandler);
  signal (SIGINT, SignalHandler);

#if 0
  pthread_create ();
#endif

  fdKeyPad = open ("/dev/keypad", O_RDONLY);
  if (fdKeyPad < 0) {
    printf ("can't open keypad\n");
    return -1;
  }

  do {
    nRetVal = read (fdKeyPad, &cKey, sizeof(cKey));
    if (nRetVal < 0) {
      break;
    }
    printf ("READ KEY %c\n", cKey);
  } while (1);
	
	return 0;
}				/* ----------  end of function main  ---------- */



