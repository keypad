.PHONY: all clean

all:
	make -C drv
	make -C test

clean:
	make -C drv $@
	make -C test $@
